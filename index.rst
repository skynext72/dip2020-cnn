.. dip2020-cnn-doc documentation master file, created by
   sphinx-quickstart on Fri Aug  6 10:46:34 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Решил собрать документацию к своему диплому "по взрослому"
https://dip2020-cnn.readthedocs.io/ru/latest/
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Annotation  
   Text
   List_of_sources
   Addition_1
   Addition_2
   Task

"Возможности"
=============

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
